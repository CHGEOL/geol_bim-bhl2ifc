# ====================================================
# PoC to read a Segment file and transform it to ifc
# ====================================================

# Source code base: http://academy.ifcopenshell.org/creating-a-simple-wall-with-property-set-and-quantity-information/
# Utils and other functions also from OGN project

import os
from os import path
import geol_bim.ifc_utils as ifcutl       # Import Methods for writing IFC
import time
import ifcopenshell
import numpy as np
import pandas as pd 
import yaml
import logging
import sys
from logging.config import dictConfig
from geol_bim.log_config import *         # Logging configuration geol_bim
from shapely.geometry import Point, LineString, MultiLineString
import openpyxl
import euclid 

timestamp = time.time()
timestring = time.strftime("%Y%m%dT%H%M%S", time.gmtime(timestamp))

# Load configuration from project
if not os.path.exists('data/output'):
    os.makedirs('data/output')
logging_configuration = log_config(path.join('data', 'output', f'{timestring}.log'))
dictConfig(logging_configuration)  # set config
logger = logging.getLogger('geol_bim')  # start logger
logger.info('Converting Borehole Data to IFC.')
StartTime = time.time()


def split_line(line, distance):
    # splits a line at a distance
    # returns [linesplit1, linesplit2] if 0 < distance < line.length)
    # returns [None, line] if distance < 0
    # returns [line, None] if distance > line.length
    if distance <= 0.0: 
        return [None, line]
    
    coords = list(line.coords)
    cumdist = 0
    for i,p in enumerate(coords):
        if i < len(coords) - 1:
            # create a segment with 2 verteces
            p1 = euclid.Point3(coords[i][0], coords[i][1], coords[i][2])
            p2 = euclid.Point3(coords[i+1][0], coords[i+1][1], coords[i+1][2])
            segment = euclid.LineSegment3(p1, p2)
            if distance <= cumdist + segment.length:
                # splitting the line on the current segment
                sphere = euclid.Sphere(p1, distance - cumdist)
                segmentPart = segment.intersect(sphere)
                splittingPoint = (round(segmentPart.p1.x,2), 
                                  round(segmentPart.p1.y,2), 
                                  round(segmentPart.p1.z,2))
                
                vertices = coords[:i+1] + [splittingPoint]
                LineSplit1 = LineString(vertices)

                vertices = [splittingPoint] + coords[i+1:]
                lineSplit2 = LineString(vertices)

                return [LineSplit1, lineSplit2]
            cumdist = cumdist + segment.length

    return [line, None] # distance > line.length


def get_linesegment(line, distanceFrom, distanceTo):
    segment = split_line(line, distanceTo)
    if segment[0] == None:
        # distanceTo < 0 
        return None
    segment = split_line(segment[0], distanceFrom)
    if segment[1] == None:
        # distanceFrom > line.length
        return None
    return segment[1]

             
try:
    # create variables from the config file
    dir = os.path.dirname(__file__)
    configfile = os.path.join(dir, 'bh2ifc_config.yml')
    def get_configfile(_configfile):
        configuration = yaml.safe_load(open(_configfile,'r'))
        _ReadBasicProjectInfo = configuration["BasicProjectInformation"]
        _ColumnNamesForSegmentDefinition = configuration["ColumnsForSegmentDefinition"]
        
        ColumnNamesFromPSetDefinition=[]
        for key, value in configuration["PropertySetDefinition"].items():
            ColumnNamesFromPSetDefinition.append(value) # get new column names 
        
        _ColumnNamesPropertySetDefinition = {}
        for i in range(len(configuration["PropertySetDefinition"])):
            _ColumnNamesPropertySetDefinition.update(ColumnNamesFromPSetDefinition[i])
        _for_propertys = configuration["PropertySetDefinition"]
        
        return(_for_propertys, _ColumnNamesForSegmentDefinition, _ColumnNamesPropertySetDefinition,_ReadBasicProjectInfo) 
    for_propertys, ColumnNamesForSegmentDefinition, ColumnNamesPropertySetDefinition, BasicProjectInfo = get_configfile(configfile)

    # create IFC-header
    template = ifcutl.create_template(BasicProjectInfo["Filename"], BasicProjectInfo["Creator"], BasicProjectInfo["ProjectName"], BasicProjectInfo["Organization"], 
        BasicProjectInfo["SchemaVersion"], minimal = False, file_descr=BasicProjectInfo["ProjectDescription"],  
        project_descr=BasicProjectInfo["ProjectDescription"], #authorization=BasicProjectInfo["Authorization"], 
        origin = list(BasicProjectInfo["ProjectOrigin"].values()), rotation=BasicProjectInfo["Rotation"], 
        georeferencing = "IfcMapConversion" if BasicProjectInfo['LoGeoRef'] == 50 
                            else "IfcGeometricRepresentationContext" if BasicProjectInfo['LoGeoRef'] == 40 
                            else "IfcSite" if BasicProjectInfo['LoGeoRef'] == 30 
                            else print("invalid level of georeferencing") and sys.exit())

    # write template to temp-file
    temp_handle, temp_filename = ifcutl.template2tempfile(template)
    ifcfile, project, owner_history, context = ifcutl.open_ifc(temp_filename)

    # create IfcSite: Site base point (IfcSite.ObjectPlacement) at the project origin (0, 0, 0)
    site, site_placement = ifcutl.create_ifcsite_with_survey_point(ifcfile, project, owner_history, context, origin = list(BasicProjectInfo["ProjectOrigin"].values()),
                                                            rotation=BasicProjectInfo["Rotation"], georef="IfcMapConversion" if BasicProjectInfo['LoGeoRef'] == 30 else "IfcGeometricRepresentationContext" if BasicProjectInfo['LoGeoRef'] == 50 else print("invalid level of georeferencing") and sys.exit(), descr=BasicProjectInfo["IfcSiteName"])
    ## Importing .xls File and naming the column names   
    BoreholeFileGeometry = pd.read_excel(os.path.join(dir,'data','input', BasicProjectInfo["BoreholeFileName"]),sheet_name="Geometry")
    BoreholeFileInterval = pd.read_excel(os.path.join(dir,'data','input', BasicProjectInfo["BoreholeFileName"]),sheet_name="Interval", skiprows=[0,2])
    ColumnNamesFromConfig = dict(ColumnNamesForSegmentDefinition, **ColumnNamesPropertySetDefinition)
    DictColumnNamesFromConfig = dict((y,x) for x,y in ColumnNamesFromConfig.items())
    BoreholeFileGeometry.columns = BoreholeFileGeometry.columns.map(DictColumnNamesFromConfig)
    BoreholeFileInterval.columns = BoreholeFileInterval.columns.map(DictColumnNamesFromConfig)
    BoreholeFileGeometry['Geometry'] = [xyz for xyz in zip(BoreholeFileGeometry.X_Coordinate,BoreholeFileGeometry.Y_Coordinate,BoreholeFileGeometry.Z_Coordinate)]
    BasepointCoordinatesList = BoreholeFileGeometry.groupby(['BoreholeID'])['Geometry'].apply(lambda x: x.tolist())
    
    # Überprüfung ob alle Werte korrekt sind
    for i in BoreholeFileGeometry.columns:
        if all(type(value) is tuple or type(value) is float or type(value) is int for value in BoreholeFileGeometry["{}".format(i)]) == True:
            pass
        else:
            sys.exit("Fehlerhafte Werte in Spalte " "{}".format(i))

    ColumnsToCheck = ['BoreholeID','DepthFrom','DepthTo','Radius']
    for i in BoreholeFileInterval.columns:
        for j in ColumnsToCheck:
            if i == j:
                if all(type(value) is tuple or type(value) is float or type(value) is int for value in BoreholeFileInterval["{}".format(i)]) == True:
                    pass
                else:
                    sys.exit("Fehlerhafte Werte in Spalte " "{}".format(i))
            else:
                continue

    # Zurodnung BHID zur Geometrie zur korrekten Berechnung der BoreholeGeometry
    BoreholeGeometryDict = {}
    for i, val in BoreholeFileGeometry.iterrows():
        if val.BoreholeID in BoreholeGeometryDict:
            BoreholeGeometryDict[val.BoreholeID].append(val.Geometry)
        else:
            BoreholeGeometryDict[val.BoreholeID] = [val.Geometry]

    # distances = BoreholeFileInterval.DepthFrom
    # points = [BoreholeGeometry.interpolate(distance) for distance in distances] 
    # new_line = LineString(points)

    for bhid, coords in BoreholeGeometryDict.items():
        BoreholeGeometry = LineString(coords)
        IfcSpaces = []
        for i, val in BoreholeFileInterval.iterrows():
            DepthFrom = float(val["DepthFrom"])
            DepthTo = float(val["DepthTo"])
            bhSegment = get_linesegment(BoreholeGeometry,DepthFrom,DepthTo)
        #while val["BoreholeID"] == key:
            if bhSegment == None:
                # no geometry available (e.g. DepthFrom/to not within geometry) --> create object without geometry
                space = ifcfile.createIfcSpace(ifcutl.create_guid(), owner_history, str(val["BoreholeID"]), "Borehole", "Borehole", None, None, None, "ELEMENT", "USERDEFINED", None)
            else:
                # simplyfying the segment geometry (2 vertices only)
                coord1 = bhSegment.coords[0]
                coord2 = bhSegment.coords[len(bhSegment.coords)-1]
                p1 = euclid.Point3(coord1[0]- BasicProjectInfo["ProjectOrigin"]["x"],coord1[1]- BasicProjectInfo["ProjectOrigin"]["y"],coord1[2]- BasicProjectInfo["ProjectOrigin"]["z"])
                p2 = euclid.Point3(coord2[0]- BasicProjectInfo["ProjectOrigin"]["x"],coord2[1]- BasicProjectInfo["ProjectOrigin"]["y"],coord2[2]- BasicProjectInfo["ProjectOrigin"]["z"])
                
                bhSegmentSimpl = euclid.LineSegment3(p1,p2)
                vector = bhSegmentSimpl.v
                Diameter_in_mm= val["Diameter_in_mm"]
                
                space_placement = ifcutl.create_ifclocalplacement(ifcfile, relative_to=site_placement)
                extrusion_placement = ifcutl.create_ifcaxis2placement3D(ifcfile, (p1.x, p1.y, p1.z), (vector.x, vector.y, vector.z), (1.0, 0.0, 0.0))
                extrusion = bhSegmentSimpl.length
                cartesianpointlist3d = ifcfile.createIfcCartesianPointList3D(((0.0,0.0-Diameter_in_mm,0.0), (0.0+Diameter_in_mm,0.0,0.0), (0.0,0.0+Diameter_in_mm,0.0), (0.0-Diameter_in_mm,0.0,0.0))) 
                arcindex1 = ifcfile.createIfcArcIndex((1,2,3))
                arcindex2 = ifcfile.createIfcArcIndex((3,4,1))
                outercurve = ifcfile.createIfcIndexedPolyCurve(cartesianpointlist3d, (arcindex1, arcindex2), False)
                ifcclosedprofile = ifcfile.createIfcArbitraryClosedProfileDef('AREA', None, outercurve)
                ifcdirection = ifcfile.createIfcDirection((0., 0., 1.))
                solid = ifcfile.createIfcExtrudedAreaSolid(ifcclosedprofile, extrusion_placement, ifcdirection, extrusion)
                body_representation =  ifcfile.createIfcShapeRepresentation(context, "Body", "AdvancedSweptSolid", [solid])
                product_shape = ifcfile.createIfcProductDefinitionShape(None, None, [body_representation])
                space = ifcfile.createIfcSpace(ifcutl.create_guid(), owner_history, str(val["BoreholeID"]), "BOREHOLE_SEGMENT", "BoreholeSegment", space_placement, product_shape, None, "ELEMENT", "USERDEFINED", None)


                # create property_sets
                PropertyKeyValues = [] # Keys and their values from the config file
                for keys, values in for_propertys.items():
                    for value in values: 
                        PropertyKeyValues.append([keys, value])   
                
                for key in for_propertys.keys():
                    PropertySingleValues = [] # list with SingleValues, values fromexample 59=IfcPropertySingleValue('volume_factor','volume_factor',IfcText('80.39329512'),$)
                    for i in PropertyKeyValues:
                        if i[0] == key:
                            PropertySingleValueWriter = ifcfile.createIfcPropertySingleValue("{}".format(i[1]), "{}".format(i[1]), ifcfile.create_entity("IfcText", str((val["{}".format(i[1])]))), None)
                            PropertySingleValues.append(PropertySingleValueWriter)
                    property_set = ifcfile.createIfcPropertySet(ifcutl.create_guid(), owner_history, "{}".format(key), None, PropertySingleValues)
                    ifcfile.createIfcRelDefinesByProperties(ifcutl.create_guid(), owner_history, None, None, [space], property_set) 
                IfcSpaces.append(space)
            
        container_space = ifcfile.createIfcRelAggregates(ifcutl.create_guid(), owner_history, "Space Container", None, site, IfcSpaces)
        ifcfile.write(BasicProjectInfo["Filename"])
    logger.info('Successfully done!')

    # time duration to calculate segments
    def timecalc(_calculatedseconds):
        seconds_in_day = 60 * 60 * 24
        seconds_in_hour = 60 * 60
        seconds_in_minute = 60
        days = _calculatedseconds // seconds_in_day
        hours = (_calculatedseconds - (days * seconds_in_day)) // seconds_in_hour
        minutes = (_calculatedseconds - (days * seconds_in_day) - (hours * seconds_in_hour)) // seconds_in_minute
        return(minutes)
    calculatedseconds = time.time() - StartTime
    logger.info('The script finished in {} minutes'.format(timecalc(calculatedseconds)))
except:
    logger.exception('Got exception on main handler')
    raise
