# GEOL_BIM-Bhl2ifc

Transformation von Bohrlochgeometrien in das IFC-Format.
 <br> 
  <br> 
 
# 1. Übersicht
Mit diesem Python-Skript werden Bohrlochdaten nach IFC transformiert. Dabei gelten folgende Rahmenbedingungen und Transformationsregeln:

* Quellformat: Die Bohrlochdaten sind im Format .xlsx bereitzustellen. 
  * Ein .xlsx Blatt enthält Metadaten
  * Ein .xlsx Blatt enthält die Geometrie -> "Geometry": 
    * Bei geraden Bohrlöchern ide Koordinaten des ersten und des letzten Messpunktes
    * Bei gekrümmten Bohrlöchern sind die Koordinaten der Knickpunkte anzugeben
    * **ACHTUNG: das Blatt, welches verwendet wird, muss "Geometry" heissen. Das heisst, der Name des Blatts in der Vorlage muss angepasst werden (entweder Geometry_gerades_Bohrloch = Geometry oder Geometry_gekrümmtes_Bohrloch = Geometry"**
  * Ein .xlsx Blatt enthält die Abschnitte -> "Interval":
    * Die Abschnitte werden in Metern angegeben und in Spalten "Tiefe von" und "Tiefe bis" gespeichert. Zu jedem Abschnitt können beliebige Informationen angehängt werden (Geologie, Lithostratigraphie etc.)
  * Die Segmente werden definiert über ihre jeweilige Mittelpunktkoordinate sowie die individuelle Ausdehnung des angegebenen Bohrlochdurchschnitts
  * Mit Hilfe der Geometrie wird ein Liniensegment kreiert. Dieses wird jeweils "zugeschnitten" an den im Blatt "Interval" angegebenen Distanzen
  * Die Koordinatenwerte der Bohrlöcher müssen im Koordinatensystem LV95 vorliegen.
  * Für jedes Segment / Bohrlochabschnitt können beliebige Eigenschaften definiert werden (als frei definierbare Spalten der xlsx-Datei). Die Transformation dieser Eigenschaften ins Datenmodell von IFC kann über die Konfiguration gesteuert werden.

  * --> Das Modell zur Darstellung Bohrlöcher und die Struktur der xlsx-Datei werden weiter unten im Detail beschrieben (Abschnitt Konfiguration).


* Zielformat: Die Bohrlöcher werden nach IFC transformiert.
  * IFC-Version: 4x1
  * Aus jedem Bohrlochabschnitt wird ein einzelner IfcSpace erzeugt. Der Objekttyp des IfcSpaces wird als 'BOREHOLE_SEGMENT' gesetzt (IfcSpace.ObjectType).
  * Die Segmente werden in der Raumstruktur direkt der IfcSite angefügt.
  * Die Art der Georeferenzierung in IFC kann gewählt werden. Es wird dazu das Konzept der "Level of Georeferencing" (LoGeoRef) verwendet und die Level 30, 40, 50 unterstützt.





## 1.1. Installation: 
 1. Download Anaconda
 2. Download Gitlab Repository
 3. Erstellen der Entwicklungssumgebung aus der yml-Datei im Installationsordner: `conda env create -f py37geol_bim.yml`
 4. Download IfcOpenShell für Python 3.7 (http://ifcopenshell.org/python), lokal ablegen: _C:\Anaconda3\envs\py37geol_bim\Lib\site-packages_
 5. Aktivieren der neuen Umgebung in der Anaconda Prompt mit dem Befehl: `conda activate py37geol_bim`


## 1.2. Benutzung:

 - Exportieren der Bohrlochstruktur aus der 3D-Software als .xlsx-Datei und diese ablegen in den Ordner "data/input"
 - Anpassung des config.yml **NUR** an den kommentierten respektive den weiter unten im Readme beschriebenen Stellen anpassen
 - Verwendung der Datei "bhl2ifc.py", für die Erstellung der IFC-Datei zur Darstellung der Bohrlöcher 

 <br>
  
 ## 1.3. Konfiguration - Verwendung .config FIle:
Der Transferprozess kann über eine Konfiguration gesteuert werden. Die Konfigurationsdatei kann an verschiedenen Stellen für die eigene Verwendung angepasst werden. Diese Variablen sind im Abschnitt `Beschreibung der Variablen aus dem config.yml` beschrieben.

Mit Hilfe der Variablen im Abschnitt "ColumnsForSegmentDefinition" des config.yml Files werden die Spaltennamen aus dem exportierten .xlsx File auf die Struktur des bhl2ifc.py Skripts angepasst. Das genaue Vorgehen wird in den folgenden zwei Abschnitten erklärt:
  
### 1.3.1. **ColumnsForSegmentDefinition**: 
Die Variablen in diesem Abschnitt sind unabdingbar. Sie dürfen nicht ergänzt werden und es darf keine dieser Variablen weggelassen werden, da sie essenziell für die Konstruktion der einzelnen Bohrlochsegmente sind. Die Variablennamen auf der linken Seite dürfen **NICHT** verändert werden. Auf der rechten Seite stehen die Spaltennamen aus dem vom Benutzer erstellten .xlsx File.
 <br>  
  -> Zuordnung der Spalte im .xlsx File welche die X-Koordinaten erhält:
 <br> 

*Beipielversion*: X_Coordinate: Center:0   
*Angepasste Version*: X_coordinate: benutzerabhängiger Spaltenname mit X-Koordinaten  <br>   
 <br> 

### 1.3.2. **PropertySetDefinition:** 
Der interne Variablenname wird hier einem Property und einem Property Set zugewiesen. Die Definitionen der Property Sets und der Propertys können in der Konfigurationsdatei frei definiert werden. Die Variablen müssen jedoch im gleichen Format, wie im Beispiel zu sehen, angegeben werden
 <br>  
 -> Definition einer eigenen Variable und Zurodnung zum PropertySet:
 <br> 
*Beispielversion*: LayerDescription: LAYERDESC<br> 
*Angepasste Version*: benutzerdefinierter Variablenname: benutzerdefinierter Spaltenname im .xlsx File 

<br> 

# 2. Beschreibung der Variablen aus dem config.yml file:
| Variable | Beschreibung |
| ----------- | ----------- |
| **BasicProjectInformation** | **Benutzerdefinierte Werte werden den vorgegbenen Variablen zugeordnet**
|BoreholeFileName| Name der Input .xlsx Datei
|Creator| Initialen / Name des Benutzers
|Filname| Dateiname des erstellten .ifc Files
|ProjectDescription| Projektbeschreibung für IfcProject
|ProjectName| Projektname 
|ProjectOrigin | Projektnullpunkt, Angabe der x-, y-, z-Koordinaten in LV95 Koordinaten| 
|Rotation| Projektrotation zum Weltkoordinatensystem für die Georeferenzierung (Drehung VON echtem Norden zur positiven y-Achse in Grad, gegen den Uhrzeigersinn (rechtshändiges kartesisches Koordinatensystem). Rotationszentrum == Ursprung. Drehung des zugrundeliegenden Projektkoordinatensystems, relativ zum wahren Norden (geografische Nordrichtung))
|SchemaVersion|IFC Version (IFC 2.3 oder IFC 4.1)
|LoGeoRef| Level of Georeferencing (= Georeferenzierungsebene). Angabe der Level 30, 40 oder 50. Weitere Informationen unter: https://jgcc.geoprevi.ro/docs/2019/10/jgcc_2019_no10_3.pdf |
|IfcSiteName|Name des Grundstücks|
| ----------- | ----------- |
| **ColumnsForSegmentDefinition** |  **Benutzerdefinierte Werte werden den vorgegbenen Variablen zugeordnet**
| {}_Coordinate | Spalte im .xlsx File mit den jeweiligen LV95- X, Y, Z- Mittelpunktkoordinaten des einzelnen Bohrlochabschnitts|
| BoreholeID | Eindeutige ID des Bohrlochs|
| DepthFrom | Starttiefe des Segments in Meter|
| DepthTo | Endtiefe des Segments in Meter|
| ----------- | ----------- |
| **PropertySetDefinition** | **Benutzerdefinierte Werte werden den benutzderdefinierten Variablen zugeordnet**
| GEOL_BIM_BoreholeCommon | Name des PropertySets|
| Diameter | Durchmesser des Bohrrohrs [mm]| 
| GeologicFeature | Angabe Unit oder Structure |
| Perspective | Angabe Hydrologie oder Geologie oder Geotechnik |
|LayerDescription| Beschreibung der erbohrten geologischen Einheit|
|NameOrClassification|Name oder Klassifzierung|
|GeologicFeature| GeologicFeature|
|ObjectType| ObjectType|
|Elastizitaetsmodul_bei_Erstbelastung | Elastizitätsmodul bei Erstbelastung [N/mm^2]
|HydraulischeLeitfaehigkeit| Hydraulische Leitfähigkeit [m/s]|
|Kohaesion| Kohäsion [N/mm^2]|
|RaumgewichtTrocken| Raumgewicht trocken [kN/m^3]|
|Reibungswinkel| Reibungswinkel [°] |
|Wassergehalt| Wassergehalt [%]|